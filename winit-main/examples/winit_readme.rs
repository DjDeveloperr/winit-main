//! The example in `winit`'s readme, adapted to use `winit-main`.

use winit_main::reexports::{
    event::{Event, WindowEvent},
    window::WindowAttributes,
};
#[cfg(feature = "proc")]
use winit_main::{
    EventLoopHandle,
    EventReceiver,
};


#[cfg(feature = "proc")]
#[winit_main::main]
fn main(event_loop: EventLoopHandle, events: EventReceiver) {
    let window = event_loop
        .create_window(WindowAttributes::default())
        .unwrap();

    for event in events.iter() {
        if matches!(
            event,
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                window_id,
            } if window_id == window.id()
        ) {
            break;
        }
    }
}

#[cfg(not(feature = "proc"))]
fn main() {
    winit_main::run(|event_loop, events| {
        let window = event_loop
            .create_window(WindowAttributes::default())
            .unwrap();

        for event in events.iter() {
            if matches!(
                event,
                Event::WindowEvent {
                    event: WindowEvent::CloseRequested,
                    window_id,
                } if window_id == window.id()
            ) {
                break;
            }
        }
    });
}
