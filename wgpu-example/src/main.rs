
use winit_main::{
    reexports::{
        event::{Event, WindowEvent},
        window::WindowAttributes,
    },
    EventLoopHandle,
    EventReceiver,
};
use wgpu::*;
use tokio::runtime::Runtime;
use tracing_subscriber::FmtSubscriber;
use anyhow::*;
use tracing::*;


async fn inner_main(event_loop: EventLoopHandle, events: EventReceiver) -> Result<()> {
    trace!("creating window, surface, and adapter");
    let window = event_loop
        .create_window(WindowAttributes::default())
        .context("Failed to create window")?;

    let size = window.inner_size();
    let instance = Instance::new(BackendBit::PRIMARY);
    let surface = unsafe { instance.create_surface(&window) };
    let adapter = instance
        .request_adapter(&RequestAdapterOptions {
            power_preference: PowerPreference::default(),
            compatible_surface: Some(&surface),
        })
        .await
        .ok_or_else(|| Error::msg("Failed to find an appropriate adapter"))?;

    trace!("creating logical device and command queue");
    let (device, queue) = adapter
        .request_device(
            &DeviceDescriptor {
                features: Features::empty(),
                limits: Limits::default(),
                shader_validation: false,
            },
            None
        )
        .await
        .context("Failed to create device")?;

    trace!("loading shader modules");
    let vs_module = device
        .create_shader_module(include_spirv!("shader.vert.spv"));
    let fs_module = device
        .create_shader_module(include_spirv!("shader.frag.spv"));

    trace!("creating render pipeline layout");
    let pipeline_layout = device
        .create_pipeline_layout(&PipelineLayoutDescriptor {
            label: None,
            bind_group_layouts: &[],
            push_constant_ranges: &[],
        });

    let swapchain_format = TextureFormat::Bgra8Unorm;

    trace!("creating render pipeline");
    let render_pipeline = device
        .create_render_pipeline(&RenderPipelineDescriptor {
            label: None,
            layout: Some(&pipeline_layout),
            vertex_stage: ProgrammableStageDescriptor {
                module: &vs_module,
                entry_point: "main",
            },
            fragment_stage: Some(ProgrammableStageDescriptor {
                module: &fs_module,
                entry_point: "main",
            }),
            rasterization_state: None,
            primitive_topology: PrimitiveTopology::TriangleList,
            color_states: &[swapchain_format.into()],
            depth_stencil_state: None,
            vertex_state: VertexStateDescriptor {
                index_format: IndexFormat::Uint16,
                vertex_buffers: &[],
            },
            sample_count: 1,
            sample_mask: !0,
            alpha_to_coverage_enabled: false,
        });

    trace!("creating swap chain");
    let mut sc_desc = SwapChainDescriptor {
        usage: TextureUsage::OUTPUT_ATTACHMENT,
        format: swapchain_format,
        width: size.width,
        height: size.height,
        present_mode: PresentMode::Mailbox,
    };

    let mut swap_chain = device.create_swap_chain(&surface, &sc_desc);

    trace!("entering event loop");
    for event in events.iter() {
        match event {
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => {
                break;
            }
            Event::WindowEvent {
                event: WindowEvent::Resized(size),
                ..
            } => {
                sc_desc.width = size.width;
                sc_desc.height = size.height;
                swap_chain = device.create_swap_chain(&surface, &sc_desc);
            }
            Event::RedrawRequested(_) => {
                let frame = swap_chain
                    .get_current_frame()
                    .context("Failed to acquire next swap chain texture")?
                    .output;
                let mut encoder = device
                    .create_command_encoder(&CommandEncoderDescriptor {
                        label: None,
                    });
                {
                    let mut rpass = encoder
                        .begin_render_pass(&RenderPassDescriptor {
                            color_attachments: &[
                                RenderPassColorAttachmentDescriptor {
                                    attachment: &frame.view,
                                    resolve_target: None,
                                    ops: Operations {
                                        load: LoadOp::Clear(Color::GREEN),
                                        store: true,
                                    },
                                },
                            ],
                            depth_stencil_attachment: None,
                        });
                    rpass.set_pipeline(&render_pipeline);
                    rpass.draw(0..3, 0..1);
                }
                queue.submit(Some(encoder.finish()));
                window.request_redraw();
            }
            _ => ()
        };
    }


    Ok(())
}

fn main() {
    let subscriber = FmtSubscriber::builder()
        //.with_max_level(Level::TRACE)
        .finish();
    tracing::subscriber::set_global_default(subscriber)
        .expect("Failed to initialize logging");

    winit_main::run(|event_loop, events| {
        let result = Runtime::new()
            .expect("Failed to create runtime")
            .block_on(inner_main(event_loop, events));
        match result {
            Result::Ok(()) => {
                info!("Exiting program");
            }
            Err(error) => {
                error!({%error}, "Exiting program")
            }
        };
    });
}
